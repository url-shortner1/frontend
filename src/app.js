import 'bootstrap/dist/css/bootstrap.min.css';
import React from 'react'
import ReactDOM from 'react-dom'
import URLShortner from './components/URLShortner'
import '../src/styles/app.css'


ReactDOM.render(<URLShortner />,document.getElementById('app'))