import React,{Component} from 'react'
import {Container,Row,Col,Button,Input,Form,FormGroup,Label,FormText} from 'reactstrap'

class MainArea extends React.Component{
    constructor(){
        super()
        this.handleClick=this.handleClick.bind(this)
        this.handleFocus=this.handleFocus.bind(this)
    }
    handleClick(e){ //handles submit 
        e.preventDefault()
        const url=e.target.elements.url.value
        if(url.length)
            this.props.handleClick(url) //send data  to parent component 
    }
    handleFocus(e){ //clear input box
        e.preventDefault()
        let inputValue=document.getElementById('urlInput').value
        let exp='(https?:\/\/(?:www\.|(?!www))[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]\.[^\s]{2,}|www\.[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]\.[^\s]{2,}|https?:\/\/(?:www\.|(?!www))[a-zA-Z0-9]+\.[^\s]{2,}|www\.[a-zA-Z0-9]+\.[^\s]{2,})'
        var regex = new RegExp(exp);
        if(inputValue.match(regex)){
            document.getElementById('urlInput').value=''
            this.props.removeCopyButton()
        }
        
    }

    render(){
    return(
    <Container>
        <Form onSubmit={this.handleClick}>
            <FormGroup>
            <Row>
                <Col lg='6' >
                    <Input onFocus={this.handleFocus} required type="text" title="URL must contain http:// or https://" pattern="https?://.+" name="url" id="urlInput" placeholder="http://" />
                </Col>
                <Col lg='4'>
                    <Button id="submit" type="submit">Submit</Button>
                </Col>
                </Row>
            </FormGroup>
        </Form>
    </Container>
    )
}
}

export default MainArea