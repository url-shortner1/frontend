import React from 'react'
import Header from './Header'
import MainArea from './Main'
import Footer from './Footer'
import Clipboard from 'react-clipboard.js';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css'

const axios = require("axios");
const BASE_URL = window.BASE_URL;


class URLShortner extends React.Component{
    constructor(props){
        super(props)
       this.state={
           url:null,
           copied:false
       }
       this.handleClick=this.handleClick.bind(this)
       this.getText=this.getText.bind(this)
       this.onSuccess=this.onSuccess.bind(this)
       this.removeCopyButton=this.removeCopyButton.bind(this)
    }
   
  handleClick(url){    //send request to get the short url from server
        axios.post(BASE_URL+'/short',{url})
        .then(res=>{
        if(res.status==200 && res.data.hasOwnProperty('shortUrl')){

            console.log(res.data.shortUrl);
            const shortUrl=res.data.shortUrl
            this.setState(()=>({url:BASE_URL+'/'+shortUrl}))
        }
        else
            return ''

    }).catch(err=>{
        console.log(err);
    })
   }

   onSuccess(){ //on successfull copy
    toast("URL Copied to Clipboard",{
    });
   }

   getText() {
    return this.state.url
  }
  removeCopyButton(){
    this.setState(()=>({url:null}))
  }
   
   render(){
       return (
        <div>
        <Header></Header>
        <MainArea handleClick={this.handleClick} removeCopyButton={this.removeCopyButton}/>
      
        {this.state.url &&
           <div className="copyDiv">
                <span><a href={this.state.url} target="_blank">{this.state.url}</a></span>
                <Clipboard  option-text={this.getText} onSuccess={this.onSuccess}>
                    Copy
                </Clipboard> 
           </div> 
        }
       <Footer />
        <ToastContainer />
        </div>
        )
    }
}

export default URLShortner